pfUI:RegisterModule("targettargettarget", "vanilla:tbc", function ()
  -- do not go further on disabled UFs
  if C.unitframes.disable == "1" then return end

  pfUI.uf.targettargettargetScanner = CreateFrame("Button",nil,UIParent)
  pfUI.uf.targettargettargetScanner:SetScript("OnUpdate", function()
      if pfUI.uf.targettargettarget.config.visible == "0" then return end
      if ( this.limit or 1) > GetTime() then return else this.limit = GetTime() + .2 end
      if UnitExists("targettargettarget") or (pfUI.unlock and pfUI.unlock:IsShown()) then
        pfUI.uf.targettargettarget:Show()
      else
        pfUI.uf.targettargettarget:Hide()
        pfUI.uf.targettargettarget.lastUnit = nil
      end
    end)

  pfUI.uf.targettargettarget = pfUI.uf:CreateUnitFrame("TargetTargetTarget", nil, C.unitframes.tttarget, .2)
  pfUI.uf.targettargettarget:UpdateFrameSize()
  pfUI.uf.targettargettarget:SetPoint("BOTTOM", UIParent , "BOTTOM", 0, 200)
  UpdateMovable(pfUI.uf.targettargettarget)
end)
